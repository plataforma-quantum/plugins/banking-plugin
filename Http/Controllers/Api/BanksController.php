<?php

namespace Plugins\Banking\Http\Controllers\Api;

use Illuminate\Routing\Controller;

class BanksController extends Controller
{

    /**
     * List all banks
     *
     */
    public function index()
    {
        $banks = _q('banking')->service('bank')->get();
        return response()->json([
            'status'  => 200,
            'success' => 'ok',
            'data'    => $banks
        ], 200);
    }

    /**
     * Get a bank by id
     *
     */
    public function show($bank)
    {
        $bank = _q('banking')->service('bank')->find($bank);
        return response()->json([
            'status'  => 200,
            'success' => 'ok',
            'data'    => $bank
        ], 200);
    }
}
