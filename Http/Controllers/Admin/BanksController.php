<?php

namespace Plugins\Banking\Http\Controllers\Admin;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class BanksController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Bancos';

    /**
     * Bank Service Instance
     *
     */
    protected $bankService;

    /**
     * Constructor Method
     *
     */
    public function __construct()
    {
        $this->bankService = _q('banking')->service('bank');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid($this->bankService->getModel());

        $grid->column('id', __('Código'))->sortable();
        $grid->column('code', __('Registro'));
        $grid->column('name', __('Banco'));
        $grid->column('created_at', __('Criado'))->date('Y-m-d H:i:s');
        $grid->column('updated_at', __('Atualizado'))->date('Y-m-d H:i:s');

        $grid->filter(function ($filter) {
            $filter->like('code', __('Código'));
            $filter->like('name', __('Banco'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show($this->bankService->findOrFail($id));

        $show->field('id', __('Código'));
        $show->field('code', __('Registro'));
        $show->field('name', __('Banco'));
        $show->field('created_at', __('Criado'));
        $show->field('updated_at', __('Atualizado'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form($this->bankService->getModel());

        $form->text('code', __('Registro'))->required();
        $form->text('name', __('Banco'))->required();

        return $form;
    }
}
