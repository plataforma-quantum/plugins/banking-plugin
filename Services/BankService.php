<?php

namespace Plugins\Banking\Services;

use Plugins\Banking\Entities\Bank;
use Quantum\Models\Service;

class BankService extends Service
{

    /**
     * Service Model Instance
     *
     */
    protected $model = Bank::class;
}
