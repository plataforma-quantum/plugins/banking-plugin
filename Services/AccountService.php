<?php

namespace Plugins\Banking\Services;

use Plugins\Banking\Entities\Account;
use Quantum\Models\Service;

class AccountService extends Service
{

    /**
     * Service Model Instance
     *
     */
    protected $model = Account::class;
}
