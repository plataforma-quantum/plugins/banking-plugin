<?php

return [

    /**
     * Plugin name
     *
     */
    'name' => 'Banking',

    /**
     * Services Intance
     *
     */
    'services' => [
        'bank' => Plugins\Banking\Services\BankService::class,
        'account' => Plugins\Banking\Services\AccountService::class,
    ]
];
