<?php

namespace Plugins\Banking\Traits;

use Plugins\Banking\Entities\Account;

trait HasAccount
{
    /**
     * Address entitites
     *
     */
    public function accounts()
    {
        return $this->morphMany(Account::class, 'accountable');
    }

    /**
     * Address entitites
     *
     */
    public function account()
    {
        return $this->morphOne(Account::class, 'accountable');
    }
}
