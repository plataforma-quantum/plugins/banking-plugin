<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankingAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banking_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('bank_id');
            $table->bigInteger('accountable_id');
            $table->string('accountable_type');
            $table->string('account_number');
            $table->string('account_digit')->nullable();
            $table->string('agency_number');
            $table->string('agency_digit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banking_accounts');
    }
}
