<?php

namespace Plugins\Banking\Database\Seeders;

use Illuminate\Database\Seeder;

class BankingDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminMenuTableSeeder::class);
        $this->call(BanksTableSeeder::class);
    }
}
