<?php

namespace Plugins\Banking\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menu')->where('plugin', 'banking')->delete();
        $parentId = DB::table('admin_menu')->insertGetId([
            'parent_id' => 0,
            'order' => 80,
            'title' => 'Financeiro',
            'plugin' => 'banking',
            'icon' => 'fa-bank',
            'uri' => NULL,
            'permission' => NULL,
            'created_at' => NULL,
            'updated_at' => '2020-06-01 17:54:28'
        ]);

        DB::table('admin_menu')->insert([
            [
                'parent_id' => $parentId,
                'order' => 1,
                'title' => 'Bancos',
                'plugin' => 'banking',
                'icon' => 'fa-bank',
                'uri' => 'banks',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 17:54:28'
            ]
        ]);
    }
}
