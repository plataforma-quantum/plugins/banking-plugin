<?php

namespace Plugins\Banking\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banking_banks')->delete();

        DB::table('banking_banks')->insert(array(
            0 =>
            array(
                'id' => 254,
                'code' => '001',
                'name' => 'BANCO DO BRASIL S/A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 =>
            array(
                'id' => 255,
                'code' => '002',
                'name' => 'BANCO CENTRAL DO BRASIL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 =>
            array(
                'id' => 256,
                'code' => '003',
                'name' => 'BANCO DA AMAZONIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 =>
            array(
                'id' => 257,
                'code' => '004',
                'name' => 'BANCO DO NORDESTE DO BRASIL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 =>
            array(
                'id' => 258,
                'code' => '007',
                'name' => 'BANCO NAC DESENV. ECO. SOCIAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 =>
            array(
                'id' => 259,
                'code' => '008',
                'name' => 'BANCO MERIDIONAL DO BRASIL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 =>
            array(
                'id' => 260,
                'code' => '020',
                'name' => 'BANCO DO ESTADO DE ALAGOAS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 =>
            array(
                'id' => 261,
                'code' => '021',
                'name' => 'BANCO DO ESTADO DO ESPIRITO SANTO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 =>
            array(
                'id' => 262,
                'code' => '022',
                'name' => 'BANCO DE CREDITO REAL DE MINAS GERAIS SA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 =>
            array(
                'id' => 263,
                'code' => '024',
                'name' => 'BANCO DO ESTADO DE PERNAMBUCO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 =>
            array(
                'id' => 264,
                'code' => '025',
                'name' => 'BANCO ALFA S/A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 =>
            array(
                'id' => 265,
                'code' => '026',
                'name' => 'BANCO DO ESTADO DO ACRE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 =>
            array(
                'id' => 266,
                'code' => '027',
                'name' => 'BANCO DO ESTADO DE SANTA CATARINA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 =>
            array(
                'id' => 267,
                'code' => '028',
                'name' => 'BANCO DO ESTADO DA BAHIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 =>
            array(
                'id' => 268,
                'code' => '029',
                'name' => 'BANCO DO ESTADO DO RIO DE JANEIRO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 =>
            array(
                'id' => 269,
                'code' => '030',
                'name' => 'BANCO DO ESTADO DA PARAIBA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 =>
            array(
                'id' => 270,
                'code' => '031',
                'name' => 'BANCO DO ESTADO DE GOIAS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 =>
            array(
                'id' => 271,
                'code' => '032',
                'name' => 'BANCO DO ESTADO DO MATO GROSSO S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 =>
            array(
                'id' => 272,
                'code' => '033',
                'name' => 'BANCO DO ESTADO DE SAO PAULO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 =>
            array(
                'id' => 273,
                'code' => '034',
                'name' => 'BANCO DO ESADO DO AMAZONAS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 =>
            array(
                'id' => 274,
                'code' => '035',
                'name' => 'BANCO DO ESTADO DO CEARA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 =>
            array(
                'id' => 275,
                'code' => '036',
                'name' => 'BANCO DO ESTADO DO MARANHAO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 =>
            array(
                'id' => 276,
                'code' => '037',
                'name' => 'BANCO DO ESTADO DO PARA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 =>
            array(
                'id' => 277,
                'code' => '038',
                'name' => 'BANCO DO ESTADO DO PARANA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 =>
            array(
                'id' => 278,
                'code' => '039',
                'name' => 'BANCO DO ESTADO DO PIAUI S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 =>
            array(
                'id' => 279,
                'code' => '041',
                'name' => 'BANCO DO ESTADO DO RIO GRANDE DO SUL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 =>
            array(
                'id' => 280,
                'code' => '047',
                'name' => 'BANCO DO ESTADO DE SERGIPE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 =>
            array(
                'id' => 281,
                'code' => '048',
                'name' => 'BANCO DO ESTADO DE MINAS GERAIS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 =>
            array(
                'id' => 282,
                'code' => '059',
                'name' => 'BANCO DO ESTADO DE RONDONIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 =>
            array(
                'id' => 283,
                'code' => '070',
                'name' => 'BANCO DE BRASILIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 =>
            array(
                'id' => 284,
                'code' => '104',
                'name' => 'CAIXA ECONOMICA FEDERAL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 =>
            array(
                'id' => 285,
                'code' => '106',
                'name' => 'BANCO ITABANCO S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 =>
            array(
                'id' => 286,
                'code' => '107',
                'name' => 'BANCO BBM S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 =>
            array(
                'id' => 287,
                'code' => '109',
                'name' => 'BANCO CREDIBANCO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 =>
            array(
                'id' => 288,
                'code' => '116',
                'name' => 'BANCO B.N.L DO BRASIL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 =>
            array(
                'id' => 289,
                'code' => '148',
                'name' => 'MULTI BANCO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 =>
            array(
                'id' => 290,
                'code' => '151',
                'name' => 'CAIXA ECONOMICA DO ESTADO DE SAO PAULO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 =>
            array(
                'id' => 291,
                'code' => '153',
                'name' => 'CAIXA ECONOMICA DO ESTADO DO R.G.SUL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 =>
            array(
                'id' => 292,
                'code' => '165',
                'name' => 'BANCO NORCHEM S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 =>
            array(
                'id' => 293,
                'code' => '166',
                'name' => 'BANCO INTER-ATLANTICO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 =>
            array(
                'id' => 294,
                'code' => '168',
                'name' => 'BANCO C.C.F. BRASIL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 =>
            array(
                'id' => 295,
                'code' => '175',
                'name' => 'CONTINENTAL BANCO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 =>
            array(
                'id' => 296,
                'code' => '184',
                'name' => 'BBA - CREDITANSTALT S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 =>
            array(
                'id' => 297,
                'code' => '199',
                'name' => 'BANCO FINANCIAL PORTUGUES',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 =>
            array(
                'id' => 298,
                'code' => '200',
                'name' => 'BANCO FRICRISA AXELRUD S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 =>
            array(
                'id' => 299,
                'code' => '201',
                'name' => 'BANCO AUGUSTA INDUSTRIA E COMERCIAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 =>
            array(
                'id' => 300,
                'code' => '204',
                'name' => 'BANCO S.R.L S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 =>
            array(
                'id' => 301,
                'code' => '205',
                'name' => 'BANCO SUL AMERICA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 =>
            array(
                'id' => 302,
                'code' => '206',
                'name' => 'BANCO MARTINELLI S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 =>
            array(
                'id' => 303,
                'code' => '208',
                'name' => 'BANCO PACTUAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 =>
            array(
                'id' => 304,
                'code' => '210',
                'name' => 'DEUTSCH SUDAMERIKANICHE BANK AG',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 =>
            array(
                'id' => 305,
                'code' => '211',
                'name' => 'BANCO SISTEMA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 =>
            array(
                'id' => 306,
                'code' => '212',
                'name' => 'BANCO MATONE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 =>
            array(
                'id' => 307,
                'code' => '213',
                'name' => 'BANCO ARBI S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 =>
            array(
                'id' => 308,
                'code' => '214',
                'name' => 'BANCO DIBENS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 =>
            array(
                'id' => 309,
                'code' => '215',
                'name' => 'BANCO AMERICA DO SUL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 =>
            array(
                'id' => 310,
                'code' => '216',
                'name' => 'BANCO REGIONAL MALCON S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 =>
            array(
                'id' => 311,
                'code' => '217',
                'name' => 'BANCO AGROINVEST S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 =>
            array(
                'id' => 312,
                'code' => '218',
                'name' => 'BBS - BANCO BONSUCESSO S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 =>
            array(
                'id' => 313,
                'code' => '219',
                'name' => 'BANCO DE CREDITO DE SAO PAULO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 =>
            array(
                'id' => 314,
                'code' => '220',
                'name' => 'BANCO CREFISUL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 =>
            array(
                'id' => 315,
                'code' => '221',
                'name' => 'BANCO GRAPHUS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 =>
            array(
                'id' => 316,
                'code' => '222',
                'name' => 'BANCO AGF BRASIL S. A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 =>
            array(
                'id' => 317,
                'code' => '223',
                'name' => 'BANCO INTERUNION S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 =>
            array(
                'id' => 318,
                'code' => '224',
                'name' => 'BANCO FIBRA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 =>
            array(
                'id' => 319,
                'code' => '225',
                'name' => 'BANCO BRASCAN S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 =>
            array(
                'id' => 320,
                'code' => '228',
                'name' => 'BANCO ICATU S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 =>
            array(
                'id' => 321,
                'code' => '229',
                'name' => 'BANCO CRUZEIRO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 =>
            array(
                'id' => 322,
                'code' => '230',
                'name' => 'BANCO BANDEIRANTES S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 =>
            array(
                'id' => 323,
                'code' => '231',
                'name' => 'BANCO BOAVISTA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 =>
            array(
                'id' => 324,
                'code' => '232',
                'name' => 'BANCO INTERPART S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 =>
            array(
                'id' => 325,
                'code' => '233',
                'name' => 'BANCO MAPPIN S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 =>
            array(
                'id' => 326,
                'code' => '234',
                'name' => 'BANCO LAVRA S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 =>
            array(
                'id' => 327,
                'code' => '235',
                'name' => 'BANCO LIBERAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 =>
            array(
                'id' => 328,
                'code' => '236',
                'name' => 'BANCO CAMBIAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 =>
            array(
                'id' => 329,
                'code' => '237',
                'name' => 'BANCO BRADESCO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 =>
            array(
                'id' => 330,
                'code' => '239',
                'name' => 'BANCO BANCRED S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 =>
            array(
                'id' => 331,
                'code' => '240',
                'name' => 'BANCO DE CREDITO REAL DE MINAS GERAIS S.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 =>
            array(
                'id' => 332,
                'code' => '241',
                'name' => 'BANCO CLASSICO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 =>
            array(
                'id' => 333,
                'code' => '242',
                'name' => 'BANCO EUROINVEST S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 =>
            array(
                'id' => 334,
                'code' => '243',
                'name' => 'BANCO STOCK S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 =>
            array(
                'id' => 335,
                'code' => '244',
                'name' => 'BANCO CIDADE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 =>
            array(
                'id' => 336,
                'code' => '245',
                'name' => 'BANCO EMPRESARIAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 =>
            array(
                'id' => 337,
                'code' => '246',
                'name' => 'BANCO ABC ROMA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 =>
            array(
                'id' => 338,
                'code' => '247',
                'name' => 'BANCO OMEGA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 =>
            array(
                'id' => 339,
                'code' => '249',
                'name' => 'BANCO INVESTCRED S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 =>
            array(
                'id' => 340,
                'code' => '250',
                'name' => 'BANCO SCHAHIN CURY S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 =>
            array(
                'id' => 341,
                'code' => '251',
                'name' => 'BANCO SAO JORGE S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 =>
            array(
                'id' => 342,
                'code' => '252',
                'name' => 'BANCO FININVEST S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 =>
            array(
                'id' => 343,
                'code' => '254',
                'name' => 'BANCO PARANA BANCO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 =>
            array(
                'id' => 344,
                'code' => '255',
                'name' => 'MILBANCO S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 =>
            array(
                'id' => 345,
                'code' => '256',
                'name' => 'BANCO GULVINVEST S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 =>
            array(
                'id' => 346,
                'code' => '258',
                'name' => 'BANCO INDUSCRED S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 =>
            array(
                'id' => 347,
                'code' => '261',
                'name' => 'BANCO VARIG S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 =>
            array(
                'id' => 348,
                'code' => '262',
                'name' => 'BANCO BOREAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 =>
            array(
                'id' => 349,
                'code' => '263',
                'name' => 'BANCO CACIQUE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 =>
            array(
                'id' => 350,
                'code' => '264',
                'name' => 'BANCO PERFORMANCE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 =>
            array(
                'id' => 351,
                'code' => '265',
                'name' => 'BANCO FATOR S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 =>
            array(
                'id' => 352,
                'code' => '266',
                'name' => 'BANCO CEDULA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 =>
            array(
                'id' => 353,
                'code' => '267',
                'name' => 'BANCO BBM-COM.C.IMOB.CFI S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 =>
            array(
                'id' => 354,
                'code' => '275',
                'name' => 'BANCO REAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 =>
            array(
                'id' => 355,
                'code' => '277',
                'name' => 'BANCO PLANIBANC S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 =>
            array(
                'id' => 356,
                'code' => '282',
                'name' => 'BANCO BRASILEIRO COMERCIAL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 =>
            array(
                'id' => 357,
                'code' => '291',
                'name' => 'BANCO DE CREDITO NACIONAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 =>
            array(
                'id' => 358,
                'code' => '294',
                'name' => 'BCR - BANCO DE CREDITO REAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 =>
            array(
                'id' => 359,
                'code' => '295',
                'name' => 'BANCO CREDIPLAN S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 =>
            array(
                'id' => 360,
                'code' => '300',
                'name' => 'BANCO DE LA NACION ARGENTINA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 =>
            array(
                'id' => 361,
                'code' => '302',
                'name' => 'BANCO DO PROGRESSO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 =>
            array(
                'id' => 362,
                'code' => '303',
                'name' => 'BANCO HNF S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 =>
            array(
                'id' => 363,
                'code' => '304',
                'name' => 'BANCO PONTUAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 =>
            array(
                'id' => 364,
                'code' => '308',
                'name' => 'BANCO COMERCIAL BANCESA S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 =>
            array(
                'id' => 365,
                'code' => '318',
                'name' => 'BANCO B.M.G. S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 =>
            array(
                'id' => 366,
                'code' => '320',
                'name' => 'BANCO INDUSTRIAL E COMERCIAL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 =>
            array(
                'id' => 367,
                'code' => '341',
                'name' => 'BANCO ITAU S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 =>
            array(
                'id' => 368,
                'code' => '346',
                'name' => 'BANCO FRANCES E BRASILEIRO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 =>
            array(
                'id' => 369,
                'code' => '347',
                'name' => 'BANCO SUDAMERIS BRASIL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 =>
            array(
                'id' => 370,
                'code' => '351',
                'name' => 'BANCO BOZANO SIMONSEN S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 =>
            array(
                'id' => 371,
                'code' => '353',
                'name' => 'BANCO GERAL DO COMERCIO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 =>
            array(
                'id' => 372,
                'code' => '356',
                'name' => 'ABN AMRO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 =>
            array(
                'id' => 373,
                'code' => '366',
                'name' => 'BANCO SOGERAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 =>
            array(
                'id' => 374,
                'code' => '369',
                'name' => 'PONTUAL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 =>
            array(
                'id' => 375,
                'code' => '370',
                'name' => 'BEAL - BANCO EUROPEU PARA AMERICA LATINA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 =>
            array(
                'id' => 376,
                'code' => '372',
                'name' => 'BANCO ITAMARATI S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 =>
            array(
                'id' => 377,
                'code' => '375',
                'name' => 'BANCO FENICIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 =>
            array(
                'id' => 378,
                'code' => '376',
                'name' => 'CHASE MANHATTAN BANK S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 =>
            array(
                'id' => 379,
                'code' => '388',
                'name' => 'BANCO MERCANTIL DE DESCONTOS S/A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 =>
            array(
                'id' => 380,
                'code' => '389',
                'name' => 'BANCO MERCANTIL DO BRASIL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 =>
            array(
                'id' => 381,
                'code' => '392',
                'name' => 'BANCO MERCANTIL DE SAO PAULO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 =>
            array(
                'id' => 382,
                'code' => '394',
                'name' => 'BANCO B.M.C. S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 =>
            array(
                'id' => 383,
                'code' => '399',
                'name' => 'BANCO BAMERINDUS DO BRASIL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 =>
            array(
                'id' => 384,
                'code' => '409',
                'name' => 'UNIBANCO - UNIAO DOS BANCOS BRASILEIROS',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 =>
            array(
                'id' => 385,
                'code' => '412',
                'name' => 'BANCO NACIONAL DA BAHIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 =>
            array(
                'id' => 386,
                'code' => '415',
                'name' => 'BANCO NACIONAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 =>
            array(
                'id' => 387,
                'code' => '420',
                'name' => 'BANCO NACIONAL DO NORTE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 =>
            array(
                'id' => 388,
                'code' => '422',
                'name' => 'BANCO SAFRA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 =>
            array(
                'id' => 389,
                'code' => '424',
                'name' => 'BANCO NOROESTE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 =>
            array(
                'id' => 390,
                'code' => '434',
                'name' => 'BANCO FORTALEZA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 =>
            array(
                'id' => 391,
                'code' => '453',
                'name' => 'BANCO RURAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 =>
            array(
                'id' => 392,
                'code' => '456',
                'name' => 'BANCO TOKIO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 =>
            array(
                'id' => 393,
                'code' => '464',
                'name' => 'BANCO SUMITOMO BRASILEIRO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 =>
            array(
                'id' => 394,
                'code' => '466',
                'name' => 'BANCO MITSUBISHI BRASILEIRO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 =>
            array(
                'id' => 395,
                'code' => '472',
                'name' => 'LLOYDS BANK PLC',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 =>
            array(
                'id' => 396,
                'code' => '473',
                'name' => 'BANCO FINANCIAL PORTUGUES S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 =>
            array(
                'id' => 397,
                'code' => '477',
                'name' => 'CITIBANK N.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 =>
            array(
                'id' => 398,
                'code' => '479',
                'name' => 'BANCO DE BOSTON S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 =>
            array(
                'id' => 399,
                'code' => '480',
                'name' => 'BANCO PORTUGUES DO ATLANTICO-BRASIL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 =>
            array(
                'id' => 400,
                'code' => '483',
                'name' => 'BANCO AGRIMISA S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 =>
            array(
                'id' => 401,
                'code' => '487',
                'name' => 'DEUTSCHE BANK S.A - BANCO ALEMAO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 =>
            array(
                'id' => 402,
                'code' => '488',
                'name' => 'BANCO J. P. MORGAN S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 =>
            array(
                'id' => 403,
                'code' => '489',
                'name' => 'BANESTO BANCO URUGAUAY S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 =>
            array(
                'id' => 404,
                'code' => '492',
                'name' => 'INTERNATIONALE NEDERLANDEN BANK N.V.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 =>
            array(
                'id' => 405,
                'code' => '493',
                'name' => 'BANCO UNION S.A.C.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 =>
            array(
                'id' => 406,
                'code' => '494',
                'name' => 'BANCO LA REP. ORIENTAL DEL URUGUAY',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 =>
            array(
                'id' => 407,
                'code' => '495',
                'name' => 'BANCO LA PROVINCIA DE BUENOS AIRES',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 =>
            array(
                'id' => 408,
                'code' => '496',
                'name' => 'BANCO EXTERIOR DE ESPANA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 =>
            array(
                'id' => 409,
                'code' => '498',
                'name' => 'CENTRO HISPANO BANCO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 =>
            array(
                'id' => 410,
                'code' => '499',
                'name' => 'BANCO IOCHPE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 =>
            array(
                'id' => 411,
                'code' => '501',
                'name' => 'BANCO BRASILEIRO IRAQUIANO S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 =>
            array(
                'id' => 412,
                'code' => '502',
                'name' => 'BANCO SANTANDER S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 =>
            array(
                'id' => 413,
                'code' => '504',
                'name' => 'BANCO MULTIPLIC S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 =>
            array(
                'id' => 414,
                'code' => '505',
                'name' => 'BANCO GARANTIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 =>
            array(
                'id' => 415,
                'code' => '600',
                'name' => 'BANCO LUSO BRASILEIRO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 =>
            array(
                'id' => 416,
                'code' => '601',
                'name' => 'BFC BANCO S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 =>
            array(
                'id' => 417,
                'code' => '602',
                'name' => 'BANCO PATENTE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 =>
            array(
                'id' => 418,
                'code' => '604',
                'name' => 'BANCO INDUSTRIAL DO BRASIL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 =>
            array(
                'id' => 419,
                'code' => '607',
                'name' => 'BANCO SANTOS NEVES S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 =>
            array(
                'id' => 420,
                'code' => '608',
                'name' => 'BANCO OPEN S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 =>
            array(
                'id' => 421,
                'code' => '610',
                'name' => 'BANCO V.R. S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 =>
            array(
                'id' => 422,
                'code' => '611',
                'name' => 'BANCO PAULISTA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 =>
            array(
                'id' => 423,
                'code' => '612',
                'name' => 'BANCO GUANABARA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 =>
            array(
                'id' => 424,
                'code' => '613',
                'name' => 'BANCO PECUNIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 =>
            array(
                'id' => 425,
                'code' => '616',
                'name' => 'BANCO INTERPACIFICO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 =>
            array(
                'id' => 426,
                'code' => '617',
                'name' => 'BANCO INVESTOR S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 =>
            array(
                'id' => 427,
                'code' => '618',
                'name' => 'BANCO TENDENCIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 =>
            array(
                'id' => 428,
                'code' => '621',
                'name' => 'BANCO APLICAP S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 =>
            array(
                'id' => 429,
                'code' => '622',
                'name' => 'BANCO DRACMA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 =>
            array(
                'id' => 430,
                'code' => '623',
                'name' => 'BANCO PANAMERICANO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 =>
            array(
                'id' => 431,
                'code' => '624',
                'name' => 'BANCO GENERAL MOTORS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 =>
            array(
                'id' => 432,
                'code' => '625',
                'name' => 'BANCO ARAUCARIA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 =>
            array(
                'id' => 433,
                'code' => '626',
                'name' => 'BANCO FICSA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 =>
            array(
                'id' => 434,
                'code' => '627',
                'name' => 'BANCO DESTAK S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 =>
            array(
                'id' => 435,
                'code' => '628',
                'name' => 'BANCO CRITERIUM S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 =>
            array(
                'id' => 436,
                'code' => '629',
                'name' => 'BANCORP BANCO COML. E. DE INVESTMENTO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 =>
            array(
                'id' => 437,
                'code' => '630',
                'name' => 'BANCO INTERCAP S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 =>
            array(
                'id' => 438,
                'code' => '633',
                'name' => 'BANCO REDIMENTO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 =>
            array(
                'id' => 439,
                'code' => '634',
                'name' => 'BANCO TRIANGULO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 =>
            array(
                'id' => 440,
                'code' => '635',
                'name' => 'BANCO DO ESTADO DO AMAPA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 =>
            array(
                'id' => 441,
                'code' => '637',
                'name' => 'BANCO SOFISA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 =>
            array(
                'id' => 442,
                'code' => '638',
                'name' => 'BANCO PROSPER S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 =>
            array(
                'id' => 443,
                'code' => '639',
                'name' => 'BIG S.A. - BANCO IRMAOS GUIMARAES',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 =>
            array(
                'id' => 444,
                'code' => '640',
                'name' => 'BANCO DE CREDITO METROPOLITANO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 =>
            array(
                'id' => 445,
                'code' => '641',
                'name' => 'BANCO EXCEL ECONOMICO S/A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 =>
            array(
                'id' => 446,
                'code' => '643',
                'name' => 'BANCO SEGMENTO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 =>
            array(
                'id' => 447,
                'code' => '645',
                'name' => 'BANCO DO ESTADO DE RORAIMA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 =>
            array(
                'id' => 448,
                'code' => '647',
                'name' => 'BANCO MARKA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 =>
            array(
                'id' => 449,
                'code' => '648',
                'name' => 'BANCO ATLANTIS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 =>
            array(
                'id' => 450,
                'code' => '649',
                'name' => 'BANCO DIMENSAO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 =>
            array(
                'id' => 451,
                'code' => '650',
                'name' => 'BANCO PEBB S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 =>
            array(
                'id' => 452,
                'code' => '652',
                'name' => 'BANCO FRANCES E BRASILEIRO SA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 =>
            array(
                'id' => 453,
                'code' => '653',
                'name' => 'BANCO INDUSVAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 =>
            array(
                'id' => 454,
                'code' => '654',
                'name' => 'BANCO A. J. RENNER S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 =>
            array(
                'id' => 455,
                'code' => '655',
                'name' => 'BANCO VOTORANTIM S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 =>
            array(
                'id' => 456,
                'code' => '656',
                'name' => 'BANCO MATRIX S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 =>
            array(
                'id' => 457,
                'code' => '657',
                'name' => 'BANCO TECNICORP S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 =>
            array(
                'id' => 458,
                'code' => '658',
                'name' => 'BANCO PORTO REAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 =>
            array(
                'id' => 459,
                'code' => '702',
                'name' => 'BANCO SANTOS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 =>
            array(
                'id' => 460,
                'code' => '705',
                'name' => 'BANCO INVESTCORP S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 =>
            array(
                'id' => 461,
                'code' => '707',
                'name' => 'BANCO DAYCOVAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 =>
            array(
                'id' => 462,
                'code' => '711',
                'name' => 'BANCO VETOR S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 =>
            array(
                'id' => 463,
                'code' => '713',
                'name' => 'BANCO CINDAM S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 =>
            array(
                'id' => 464,
                'code' => '715',
                'name' => 'BANCO VEGA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 =>
            array(
                'id' => 465,
                'code' => '718',
                'name' => 'BANCO OPERADOR S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 =>
            array(
                'id' => 466,
                'code' => '719',
                'name' => 'BANCO PRIMUS S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 =>
            array(
                'id' => 467,
                'code' => '720',
                'name' => 'BANCO MAXINVEST S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 =>
            array(
                'id' => 468,
                'code' => '721',
                'name' => 'BANCO CREDIBEL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 =>
            array(
                'id' => 469,
                'code' => '722',
                'name' => 'BANCO INTERIOR DE SAO PAULO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 =>
            array(
                'id' => 470,
                'code' => '724',
                'name' => 'BANCO PORTO SEGURO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 =>
            array(
                'id' => 471,
                'code' => '725',
                'name' => 'BANCO FINABANCO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 =>
            array(
                'id' => 472,
                'code' => '726',
                'name' => 'BANCO UNIVERSAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 =>
            array(
                'id' => 473,
                'code' => '728',
                'name' => 'BANCO FITAL S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 =>
            array(
                'id' => 474,
                'code' => '729',
                'name' => 'BANCO FONTE S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 =>
            array(
                'id' => 475,
                'code' => '730',
                'name' => 'BANCO COMERCIAL PARAGUAYO S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 =>
            array(
                'id' => 476,
                'code' => '731',
                'name' => 'BANCO GNPP S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 =>
            array(
                'id' => 477,
                'code' => '732',
                'name' => 'BANCO PREMIER S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 =>
            array(
                'id' => 478,
                'code' => '733',
                'name' => 'BANCO NACOES S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 =>
            array(
                'id' => 479,
                'code' => '734',
                'name' => 'BANCO GERDAU S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 =>
            array(
                'id' => 480,
                'code' => '735',
                'name' => 'BACO POTENCIAL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 =>
            array(
                'id' => 481,
                'code' => '736',
                'name' => 'BANCO UNITED S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 =>
            array(
                'id' => 482,
                'code' => '737',
                'name' => 'THECA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 =>
            array(
                'id' => 483,
                'code' => '738',
                'name' => 'MARADA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 =>
            array(
                'id' => 484,
                'code' => '739',
                'name' => 'BGN',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 =>
            array(
                'id' => 485,
                'code' => '740',
                'name' => 'BCN BARCLAYS',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 =>
            array(
                'id' => 486,
                'code' => '741',
                'name' => 'BRP',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 =>
            array(
                'id' => 487,
                'code' => '742',
                'name' => 'EQUATORIAL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 =>
            array(
                'id' => 488,
                'code' => '743',
                'name' => 'BANCO EMBLEMA S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 =>
            array(
                'id' => 489,
                'code' => '744',
                'name' => 'THE FIRST NATIONAL BANK OF BOSTON',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 =>
            array(
                'id' => 490,
                'code' => '745',
                'name' => 'CITIBAN N.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 =>
            array(
                'id' => 491,
                'code' => '746',
                'name' => 'MODAL SA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 =>
            array(
                'id' => 492,
                'code' => '747',
                'name' => 'RAIBOBANK DO BRASIL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 =>
            array(
                'id' => 493,
                'code' => '748',
                'name' => 'SICREDI',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 =>
            array(
                'id' => 494,
                'code' => '749',
                'name' => 'BRMSANTIL SA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 =>
            array(
                'id' => 495,
                'code' => '750',
                'name' => 'BANCO REPUBLIC NATIONAL OF NEW YORK (BRA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 =>
            array(
                'id' => 496,
                'code' => '751',
                'name' => 'DRESDNER BANK LATEINAMERIKA-BRASIL S/A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 =>
            array(
                'id' => 497,
                'code' => '752',
                'name' => 'BANCO BANQUE NATIONALE DE PARIS BRASIL S',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 =>
            array(
                'id' => 498,
                'code' => '753',
                'name' => 'BANCO COMERCIAL URUGUAI S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 =>
            array(
                'id' => 499,
                'code' => '755',
                'name' => 'BANCO MERRILL LYNCH S.A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 =>
            array(
                'id' => 500,
                'code' => '756',
                'name' => 'BANCO COOPERATIVO DO BRASIL S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 =>
            array(
                'id' => 501,
                'code' => '757',
                'name' => 'BANCO KEB DO BRASIL S.A.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }
}
