<?php

namespace Plugins\Banking\Entities;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * Model Table Name
     *
     */
    protected $table = "banking_accounts";

    /**
     * Guarded Model Properties
     *
     */
    protected $guarded = [];

    /**
     * BelongsTo Bank
     *
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
}
