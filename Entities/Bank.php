<?php

namespace Plugins\Banking\Entities;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * Table on database
     *
     */
    protected $table = 'banking_banks';

    /**
     * Guarded properties
     *
     */
    protected $guarded = [];

    /**
     * HasMany Accounts
     *
     */
    public function accounts()
    {
        return $this->hasMany(Account::class);
    }
}
